Cross-site Scripting
#
`Cross-site Scripting 이란`
Cross-Site Scripting은 악성 JavaScript가 다른 사용자에 의해 실행될 의도로 웹 애플리케이션에 주입되는 주입 공격으로 분류됩니다.

다양한 사이트의 xss
- [shopify xss](https://hackerone.com/reports/415484)
- [steam chatting xss](https://hackerone.com/reports/409850)
- [instagram xss](https://hackerone.com/reports/283825)
- [hackerone xss](https://hackerone.com/reports/449351)

#
`XSS payload`

XSS에서 페이로드는 대상 컴퓨터에서 실행하려는 JavaScript 코드입니다. 페이로드에는 intention(의도)와 modification(수정)이라는 두 부분이 있습니다. 의도는 JavaScript가 실제로 수행하기를 바라는 것이며, 수정은 모든 시나리오가 다르기 때문에 코드를 실행하는 데 필요한 변경 사항입니다.




