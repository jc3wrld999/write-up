수동적 정찰
#
1.  whois

WHOIS는 RFC 3912 사양 을 따르는 요청 및 응답 프로토콜이다 . WHOIS 서버 는 들어오는 요청에 대해 TCP 포트 43에서 수신 대기 한다. 도메인 등록 대행자는 임대하는 도메인 이름에 대한 WHOIS 기록을 유지 관리할 책임이 있다.

응답하는 정보

- 등록 기관: 어느 등록 기관을 통해 도메인 이름이 등록되었습니까?
- 등록자의 연락처 정보: 무엇보다도 이름, 조직, 주소, 전화. (프라이버시 서비스를 통해 숨기지 않는 한)
- 생성, 업데이트 및 만료 날짜: 도메인 이름은 언제 처음 등록되었습니까? 언제 마지막으로 업데이트 되었습니까? 그리고 언제 갱신해야 합니까?
- 이름 서버: 도메인 이름 확인을 요청할 서버는 무엇입니까?

이 정보를 얻으려면 whois클라이언트나 온라인 서비스 를 사용해야 한다 . 많은 온라인 서비스에서 whois정보를 제공한다. 그러나 일반적으로 로컬 후이즈 클라이언트를 사용하는 것이 더 빠르고 편리하다. 

터미널에서 후이즈 클라이언트에 쉽게 액세스할 수 있다. 
구문은 `whois DOMAIN_NAME`이다 여기서 DOMAIN_NAME은 추가 정보를 얻으려는 도메인이다. 

```
┌──(root💀kali)-[~]
└─# whois tryhackme.com                                                                                                                                                                                                                127 ⨯
   Domain Name: TRYHACKME.COM
   Registry Domain ID: 2282723194_DOMAIN_COM-VRSN
   Registrar WHOIS Server: whois.namecheap.com
   Registrar URL: http://www.namecheap.com
   Updated Date: 2021-05-01T19:43:23Z
   Creation Date: 2018-07-05T19:46:15Z
   Registry Expiry Date: 2027-07-05T19:46:15Z
   Registrar: NameCheap, Inc.
   Registrar IANA ID: 1068
   Registrar Abuse Contact Email: abuse@namecheap.com
   Registrar Abuse Contact Phone: +1.6613102107
   Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
   Name Server: KIP.NS.CLOUDFLARE.COM
   Name Server: UMA.NS.CLOUDFLARE.COM
   DNSSEC: unsigned
   URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of whois database: 2022-01-04T07:00:14Z <<<
```

2. nslookup
Name Server Look Up를 의미

도메인이름의 ip주소를 찾음 예: `nslookup OPTIONS DOMAIN_NAME SERVER`

options


| A | IPv4 Addresses |
| --- | --- |
| AAAA | IPv6 Addresses |
| CNAME | Canonical Name |
| MX | Mail Servers |
| SOA | Start of Authority |
| TXT | TXT Records |

SERVER는 쿼리할 DNS 서버입니다. 쿼리할 로컬 또는 공용 DNS 서버를 선택할 수 있다. 클라우드플레어는 1.1.1.1과 1.0.0.1, 구글은 8.8.8과 8.8.4.4, 쿼드9은 9.9.9와 149.112.112.112를 제공한다. ISP의 DNS 서버를 대체하려는 경우 선택할 수 있는 공용 DNS 서버가 더 많다.

다음 명령어는 tryhackme.com에서 사용하는 모든 IPv4 주소를 반환하는 데 사용할 수 있다.
```
nslookup -type=A tryhackme.com 1.1.1.1
```
3개의 IPv4를 얻었다.
```
Non-authoritative answer:
printsection()
Name:   tryhackme.com
Address: 104.22.55.228
Name:   tryhackme.com
Address: 104.22.54.228
Name:   tryhackme.com
Address: 172.67.27.10
```

특정 도메인의 이메일 서버 및 구성에 대해 배우고 싶을때 `nslookup -type=MX tryhackme.com`
mail 구성을 google로 하고 있음을 알 수 있다.
```
Non-authoritative answer:
printsection()
tryhackme.com   mail exchanger = 5 alt1.aspmx.l.google.com.
tryhackme.com   mail exchanger = 10 alt3.aspmx.l.google.com.
tryhackme.com   mail exchanger = 1 aspmx.l.google.com.
tryhackme.com   mail exchanger = 5 alt2.aspmx.l.google.com.
tryhackme.com   mail exchanger = 10 alt4.aspmx.l.google.com.

```
MX는 Mail Exchange 서버를 조회하고 있으므로, 메일 서버가 email@tryhackme.com을 배달하려고 할 때 순서가 1인 aspmx.l.google.com에 연결을 시도한다. 사용 중이거나 사용할 수 없는 경우, 메일 서버는 다음 메일 교환 서버인 alt1.aspmx.l.google.com 또는 alt2.aspmx.l.google.com에 연결을 시도한다.

Google은 나열된 메일 서버를 제공하므로 메일 서버가 취약한 서버 버전을 실행한다고 예상해서는 안된다. 그러나 경우에 따라 보안 또는 패치가 제대로 되지 않은 메일 서버가 발견될 수도 있다.

이러한 정보는 목표물에 대한 수동적인 정찰을 계속할 때 유용하게 사용될 수 있다. 다른 도메인 이름에 대해 유사한 쿼리를 반복하고 -type=txt와 같은 다른 유형을 시도할 수 있다.

`dig`

고급 DNS 쿼리 및 추가 기능에 대해서는 "Domain Information Groper"의 약자인 dig를 사용할 수 있다. dig를 사용하여 MX 레코드를 찾아 nslookup과 비교해 보겠다. `dig DOMAIN_NAME`을 사용할 수 있지만 레코드 유형을 지정하려면 `dig DOMAIN_NAME TYPE`을 사용한다. 선택적으로 `dig @SERVER DOMAIN_NAME TYPE`을 사용하여 쿼리할 서버를 선택할 수 있다.

- SERVER는 쿼리할 DNS 서버
- DOMAIN_NAME이(가) 조회 중인 도메인 이름
- TYPE에는 앞에서 제공된 표에 표시된 것처럼 DNS 레코드 유형이 포함됨

```
┌──(root💀kali)-[~]
└─# ┌──(root💀kali)-[~]
└─# dig tryhackme.com MX

; <<>> DiG 9.17.21-1-Debian <<>> tryhackme.com MX
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 11375
;; flags: qr rd ra; QUERY: 1, ANSWER: 5, AUTHORITY: 2, ADDITIONAL: 13

;; QUESTION SECTION:
;tryhackme.com.                 IN      MX

;; ANSWER SECTION:
tryhackme.com.          5       IN      MX      5 alt2.aspmx.l.google.com.
tryhackme.com.          5       IN      MX      10 alt4.aspmx.l.google.com.
tryhackme.com.          5       IN      MX      10 alt3.aspmx.l.google.com.
tryhackme.com.          5       IN      MX      1 aspmx.l.google.com.
tryhackme.com.          5       IN      MX      5 alt1.aspmx.l.google.com.

;; AUTHORITY SECTION:
tryhackme.com.          5       IN      NS      kip.ns.cloudflare.com.
tryhackme.com.          5       IN      NS      uma.ns.cloudflare.com.

;; ADDITIONAL SECTION:
alt4.aspmx.l.google.com. 5      IN      A       142.250.152.26
kip.ns.cloudflare.com.  5       IN      A       172.64.33.128
kip.ns.cloudflare.com.  5       IN      A       173.245.59.128
kip.ns.cloudflare.com.  5       IN      A       108.162.193.128
uma.ns.cloudflare.com.  5       IN      A       108.162.192.146
uma.ns.cloudflare.com.  5       IN      A       172.64.32.146
uma.ns.cloudflare.com.  5       IN      A       173.245.58.146
kip.ns.cloudflare.com.  5       IN      AAAA    2803:f800:50::6ca2:c180
kip.ns.cloudflare.com.  5       IN      AAAA    2a06:98c1:50::ac40:2180
kip.ns.cloudflare.com.  5       IN      AAAA    2606:4700:58::adf5:3b80
uma.ns.cloudflare.com.  5       IN      AAAA    2606:4700:50::adf5:3a92
uma.ns.cloudflare.com.  5       IN      AAAA    2803:f800:50::6ca2:c092
uma.ns.cloudflare.com.  5       IN      AAAA    2a06:98c1:50::ac40:2092

;; Query time: 7 msec
;; SERVER: 192.168.254.2#53(192.168.254.2) (UDP)
;; WHEN: Tue Jan 04 06:40:34 EST 2022
;; MSG SIZE  rcvd: 476
```

```
┌──(root💀kali)-[~]
└─# dig @1.1.1.1 tryhackme.com MX                                                                                                                                                                                                      130 ⨯

; <<>> DiG 9.17.21-1-Debian <<>> @1.1.1.1 tryhackme.com MX
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56539
;; flags: qr rd ra; QUERY: 1, ANSWER: 5, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;tryhackme.com.                 IN      MX

;; ANSWER SECTION:
tryhackme.com.          300     IN      MX      5 alt1.aspmx.l.google.com.
tryhackme.com.          300     IN      MX      1 aspmx.l.google.com.
tryhackme.com.          300     IN      MX      10 alt4.aspmx.l.google.com.
tryhackme.com.          300     IN      MX      10 alt3.aspmx.l.google.com.
tryhackme.com.          300     IN      MX      5 alt2.aspmx.l.google.com.

;; Query time: 104 msec
;; SERVER: 1.1.1.1#53(1.1.1.1) (UDP)
;; WHEN: Tue Jan 04 06:41:52 EST 2022
;; MSG SIZE  rcvd: 157
```

첫번째 플래그
```
┌──(root💀kali)-[~]
└─# dig thmlabs.com txt

; <<>> DiG 9.17.21-1-Debian <<>> thmlabs.com txt
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 34459
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 2, ADDITIONAL: 13

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; MBZ: 0x0005, udp: 1232
; COOKIE: 1112b2907c8beaa3176c4fa361d4335117318ef20dfa9c15 (good)
;; QUESTION SECTION:
;thmlabs.com.                   IN      TXT

;; ANSWER SECTION:
thmlabs.com.            5       IN      TXT     "THM{a5b83929888ed36acb0272971e438d78}"
```

2. DNSDumpster

nslookup 및 dig와 같은 DNS 검색 도구는 자체에서 하위 도메인을 찾을 수 없다. 예를 들어, tryhackme.com에 wiki.tryhackme.com, wiki.tryhackme.com 하위 도메인이 있는 경우 대상에 대한 다양한 정보를 저장할 수 있으므로 이 두 가지에 대해 자세히 알아보려고 한다. 이러한 하위 도메인 중 하나가 설정되었으며 정기적으로 업데이트되지 않을 수 있다. 제대로 된 정기 업데이트가 없으면 대개 서비스가 취약해진다. 하지만 그러한 하위 도메인이 존재한다는 것을 어떻게 알 수 있을까?

여러 검색 엔진을 사용하여 공개적으로 알려진 하위 도메인 목록을 컴파일하는 것을 고려할 수 있다. 하나의 검색 엔진으로는 충분하지 않을 것이다. 게다가, 우리는 흥미로운 데이터를 찾기 위해 적어도 수십 개의 결과를 검토할 것을 예상해야 한다. 결국 명시적으로 보급되지 않은 하위 도메인을 찾는 것이므로 검색 결과의 첫 번째 페이지로 이동할 필요가 없다. 이러한 하위 도메인을 검색하는 또 다른 방법은 DNS 레코드가 있는 하위 도메인을 찾기 위해 무차별 강제 조회에 의존하는 것이다.

이러한 시간 소모적인 검색을 피하기 위해 DNSDumpster와 같은 DNS 쿼리에 대한 자세한 답변을 제공하는 온라인 서비스를 사용할 수 있다. DNSDumpster에서 tryhackme.com을 검색하면 일반적인 DNS 쿼리가 제공할 수 없는 하위 도메인 blog.tryhackme.com이 검색된다. 또한 DNSDumpster는 수집된 DNS 정보를 읽기 쉬운 테이블과 그래프로 반환한다. DNSDumpster는 수신 서버에 대한 수집된 정보도 제공한다.

DNSDumpster는 도메인 이름을 IP 주소로 확인하고 지리적 위치를 지정하려고 시도하기도 했다. 또한 MX 레코드를 볼 수 있다. DNSDumpster는 5개의 메일 교환 서버를 모두 각각의 IP 주소로 확인했으며 소유자와 위치에 대한 자세한 정보를 제공한다. 드디어, 우리는 TXT 기록을 볼 수 있다. 사실상 이 모든 정보를 검색하기 위해서는 한 번의 쿼리만으로도 충분했다.

![image](https://user-images.githubusercontent.com/61821641/148055763-7355394b-e595-4b06-8167-6069745ec11e.png)

![image](https://user-images.githubusercontent.com/61821641/148055823-f5304b00-5a25-4c34-9518-d9aef62898c5.png)

![image](https://user-images.githubusercontent.com/61821641/148055866-071eafd1-c37b-4e40-9aff-ad9864a5c946.png)

![image](https://user-images.githubusercontent.com/61821641/148055905-4c71571b-cc81-42fb-b033-0d6c1454c3d5.png)

![image](https://user-images.githubusercontent.com/61821641/148055974-475072e9-6a0b-436c-ba2e-65feb141cedd.png)

3. shodan.io

수동 정찰 단계의 일부로 특정 대상에 대한 침투 테스트를 실행해야 할 때 Shodan.io 와 같은 서비스는 적극적으로 연결하지 않고도 클라이언트 네트워크에 대한 다양한 정보를 배우는 데 도움이 될 수 있다. 또한 방어적인 측면에서 Shodan.io의 다양한 서비스를 사용하여 조직에 속한 연결 및 노출된 장치에 대해 알아볼 수 있다.

Shodan.io는 웹 페이지용 검색 엔진과 달리 연결된 "사물"의 검색 엔진을 구축하기 위해 온라인으로 연결할 수 있는 모든 장치에 연결하려고 한다. 응답을 받으면 서비스와 관련된 모든 정보를 수집하고 검색 가능하도록 데이터베이스에 저장한다. tryhackme.com 서버 중 하나의 저장된 기록을 고려해보면 된다.

- IP 주소
- 호스팅 회사
- 지리적 위치
- 서버 유형 및 버전

![image](https://user-images.githubusercontent.com/61821641/148057272-49d5bd80-2937-420e-b8dd-ec773a915b5d.png)

# 
요약

검색 옵션을 숙달하고 결과를 읽는 데 익숙해지면 이러한 도구를 사용하여 찾을 수 있는 정보의 양이 방대할 수 있다.
| WHOIS 기록 조회 | whois tryhackme.com |
| --- | --- |
| DNS A 레코드 조회 | nslookup -type=A tryhackme.com |
| DNS 서버에서 DNS MX 레코드 조회 | nslookup -type=MX tryhackme.com 1.1.1.1 |
| DNS TXT 레코드 조회 | nslookup -type=TXT tryhackme.com |
| DNS A 레코드 조회 | dig tryhackme.com A |
| DNS 서버에서 DNS MX 레코드 조회 | dig @1.1.1.1 tryhackme.com MX |
| DNS TXT 레코드 조회 | dig tryhackme.com TXT |

#
reference
- [rfc 3912](https://www.ietf.org/rfc/rfc3912.txt)
