network service
#
SMB (Server Message Block protocol)

네트워크의 파일, 프린터, 직렬 포트, 기타 리소스에 대한 액세스를 공유하는데 사용되는 클라이언트-서버 통신 프로토콜이다.

서버는 파일 시스템, 기타 리소스(프린터, named 파이프, API)를 네트워크의 클라이언트에서 사용할 수 있도록 한다. 클라이언트 컴퓨터에는 자체 하드 디스크가 있을 수 있지만 서버의 공유 파일 시스템 및 프린터에 대한 액세스도 원한다.

SMB 프로토콜은 응답 요청 프로토콜로 알려져 있다. 연결을 설정하기 위해 클라이언트와 서버 간에 여러 메시지를 전송한다. 클라이언트는 TCP/IP(실제로는 RFC1001 및 RFC1002에 지정된 TCP/IP를 통한 NetBIOS), NetBEUI 또는 IPX/SPX를 사용하여 서버에 연결한다.

SMB의 작동방식

![image](https://user-images.githubusercontent.com/61821641/148779138-80c3debf-36ed-46fb-b888-07018d34e024.png)

SMB를 실행하는 OS

Windows 95이후의 MS 운영체제, UNIX 시스템의 오픈소스 SAMBA

