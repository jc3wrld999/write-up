Reversing ELF
#
`crackme1`
```
chmod +x crackme1
./crackme1
```
![image](https://user-images.githubusercontent.com/61821641/150210238-fce09749-2c6e-4b5b-975c-30d8d0d9dbd9.png)
#
`crackme2`
```
./crackme2
```
![image](https://user-images.githubusercontent.com/61821641/150210163-5fa21955-e637-4150-9ced-5538f109f948.png)


strings 명령어를 사용하여 crackme2내의 텍스트를 가져오자.
```
strings crackme2
```
![image](https://user-images.githubusercontent.com/61821641/150210785-eac91a3b-10f3-467d-84db-98a4d014f4fc.png)

super_secret_password를 패스워드로 다시 실행해보자.
```
./crackme2 super_secret_password
```
![image](https://user-images.githubusercontent.com/61821641/150211142-88b86e9f-e8fe-4ae1-a71e-4bcce2283fab.png)
#
`crackme3`

strings 명령어를 실행하면 base64로 인코딩된 문자열이보인다.
```
strings crackme3
```
![image](https://user-images.githubusercontent.com/61821641/150211379-b60edb28-9d42-45e4-a1a7-7139faa69451.png)

디코딩하면 플래그가 보인다.
```
echo "ZjByX3kwdXJfNWVjMG5kX2xlNTVvbl91bmJhc2U2NF80bGxfN2gzXzdoMW5nNQ==" | base64 --decode
```
![image](https://user-images.githubusercontent.com/61821641/150211931-6469fbda-4788-4dd1-8fef-da18af03bb1e.png)
#
`crackme4`

strcmp: 두 문자열을 비교하여 같으면 0을 리턴한다.

![image](https://user-images.githubusercontent.com/61821641/150213898-dda90134-62cf-412f-8df9-abda82209e37.png)

radare를 디버그모드에서 실행해서 바이너리를 시작한다.
```
r2 -d crackme4
```
바이너리를 분석하고 함수 리스트를 얻는다.
```
aaa
afl
```
![image](https://user-images.githubusercontent.com/61821641/150215419-b8e4d172-bd80-485c-b422-ee31a8d48dd6.png)

많은 함수를 실행하고 있지만 가장 중요한 것은 main 함수다.
```
pdf @main
```
![image](https://user-images.githubusercontent.com/61821641/150216397-51942d71-95c2-470a-b484-f33702258006.png)

main함수가 반환되기전 sym.compare_pwd 함수를 호출하는 것을 알 수 있다.
```
pdf @sym.compare_pwd
```
![image](https://user-images.githubusercontent.com/61821641/150216171-464b781e-e2d4-4e6f-b4a2-325b4ea85811.png)

```
0x004006d5      e846feffff     call sym.imp.strcmp
```
0x004006d5 주소에서 strcmp 함수를 볼 수 있다. 
```
0x004006d2      4889c7         mov rdi, rax
0x004006d5      e846feffff     call sym.imp.strcmp
0x004006da      85c0           test eax, eax
0x004006dc      750c           jne 0x4006ea
```
rdi 레지스터 문자열과 rax 레지스터 문자열을 비교해서 eax = 0이면 암호확인 메세지를 리턴한다. 여기서 우리는 rax에 있는 문자열을 구해야한다. mov rdi ras에 break point를 설정한 뒤 다시 디버깅해보자. 

프로그램에 인수를 제공해야 하므로 다음 명령어를 사용한다.(''안의 값은 중요하지 않다.)
```
ood 'random'
```
```
db 0x004006d2
dc
```
![image](https://user-images.githubusercontent.com/61821641/150222227-ce5aa591-4dd7-4efc-99ba-5fa2b410feeb.png)

```
px @rdi
```
![image](https://user-images.githubusercontent.com/61821641/150219526-0c2107b4-cae7-4a93-a934-2346fe296785.png)

![image](https://user-images.githubusercontent.com/61821641/150219718-7967a8be-4d29-4883-8600-d6b55268bca8.png)

#
`crackme5`

![image](https://user-images.githubusercontent.com/61821641/150219981-039467c0-e0ba-48d4-aa8b-da523f51a648.png)

```
0x0040082c      4889c7         mov rdi, rax
0x0040082f      e8a2feffff     call sym.strcmp_
```
0x0040082c에 break point를 설정하고 디버깅한다.
```
db 0x0040082c
dc
```

![image](https://user-images.githubusercontent.com/61821641/150223135-e57ebddd-fca4-4fd0-8e38-e15e15dceee6.png)

![image](https://user-images.githubusercontent.com/61821641/150223824-d32a0379-7d53-4dec-82f5-7b41734ce09d.png)

#
`crackme6`

![image](https://user-images.githubusercontent.com/61821641/150224834-603de196-7da2-4bf3-81d6-d04b243036eb.png)

main

![image](https://user-images.githubusercontent.com/61821641/150225446-ba7a857a-0ec0-4d63-9632-5662b44c4f15.png)

sym.compare_pwd

![image](https://user-images.githubusercontent.com/61821641/150226415-9d5525b0-9286-4b78-8424-13e0a719b1b4.png)

sym.my_secure_test

![image](https://user-images.githubusercontent.com/61821641/150226846-aec2ca70-8b6f-43af-b396-64bb86caa610.png)

그래프 모드로 분석해보자.
```
agv @sym.my_secure_test
```
![image](https://user-images.githubusercontent.com/61821641/150230006-c2f1a774-ff38-4410-a0a0-370c52f75b32.png)

아래의 프로세스에서 이 함수가 rax register(al)를 16진수 값과 비교하고 같으면 rax register를 증가 시키고 다른 16진수 값과 비교한다는 것을 알 수 있다. 
![image](https://user-images.githubusercontent.com/61821641/150230475-249deaa9-dc07-4e58-9fcd-13a348bafb88.png)

![image](https://user-images.githubusercontent.com/61821641/150230541-6d1ff8d5-a0b5-4505-8acf-d74608869aa7.png)

![image](https://user-images.githubusercontent.com/61821641/150230475-249deaa9-dc07-4e58-9fcd-13a348bafb88.png)
![image](https://user-images.githubusercontent.com/61821641/150231056-d950cb2b-0e3e-4b03-b6a1-ecb9324cf2e9.png)
![image](https://user-images.githubusercontent.com/61821641/150231120-015a6902-9bc2-43ba-9c9c-1c0854573ec2.png)
![image](https://user-images.githubusercontent.com/61821641/150231171-db5ba0ab-0a6c-43a5-8880-c54e2731330b.png)
![image](https://user-images.githubusercontent.com/61821641/150231221-8b504384-f087-4021-9ef3-6538dd1d9ad0.png)
![image](https://user-images.githubusercontent.com/61821641/150231265-2a04025c-fe0f-42c5-a6ab-4a053ee1faf2.png)
![image](https://user-images.githubusercontent.com/61821641/150231294-a9a4aebb-cb72-402b-a252-e4908203a53d.png)
![image](https://user-images.githubusercontent.com/61821641/150231322-3b579911-842e-4091-b06d-46546eb2f97f.png)

모든 값을 결합하여(313333375f707764) ASCII로 디코딩하면 암호를 얻을 수 있다.

```
python3
bytes.fromhex('313333375f707764').decode('utf8')
```
![image](https://user-images.githubusercontent.com/61821641/150232326-430e183e-c8d0-4f70-bc83-3ea37474b93b.png)

![image](https://user-images.githubusercontent.com/61821641/150232446-1b0df539-0e18-4570-b7e2-eb853d9f79e3.png)
#
`crackme7`

```
0x08048665      3d697a0000     cmp eax, 0x7a69
0x0804866a      7517           jne 0x8048683
0x0804866c      83ec0c         sub esp, 0xc
0x0804866f      68bc880408     push str.Wow_such_h4x0r_    ; 0x80488bc ; "Wow such h4x0r!"
```

```
python3
string = '7a69'
decode = int(string, 16)
print(decode)
```


"Wow such h4x0r" 문자열이 호출되는 위치를 주목하자. eax와 0x7a69를 비교한 후 호출된다.

![image](https://user-images.githubusercontent.com/61821641/150234497-e41a55a9-c7b5-4378-bff6-ff78d3f81851.png)

![image](https://user-images.githubusercontent.com/61821641/150234574-feb028db-ba08-46ea-9847-c6d1ad14c63e.png)

ASCII로 디코딩한 결과를 입력하니 Unknown input! 이라는 메세지가 표시된다.

![image](https://user-images.githubusercontent.com/61821641/150234111-5766ed69-3464-4851-a8d7-a87a45c61687.png)

![image](https://user-images.githubusercontent.com/61821641/150234237-d080b655-86de-4912-ba34-5c7afbf86e46.png)

10진수로 디코딩한 결과를 입력하니 플래그를 얻을 수 있었다.
#
`crackme8`

![image](https://user-images.githubusercontent.com/61821641/150235727-57a6cad3-50c2-48ba-930b-f3dbb176b01e.png)

```
0x080484dc      e89ffeffff     call sym.imp.atoi
0x080484e1      83c410         add esp, 0x10
0x080484e4      3d0df0feca     cmp eax, 0xcafef00d
...
0x08048505      6883860408     push str.Access_granted.
```
atoi 함수에 주목하자. atoi 함수는 입력 문자열을 정수로 변환한다. 그런 다음 eax와 cafef00d를 비교하고 같으면 Access_granted 메세지를 보여준다.
