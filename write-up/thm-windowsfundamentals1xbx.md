윈도우 기초
#
kali linux에 연결하기 위해 remmina를 설치한다.
```
apt install remmina
```
remmina가 실행되면 왼쪽 상단에 +버튼을 클릭하고 아래와 같이 정보를 채워주자.

![image](https://user-images.githubusercontent.com/61821641/148638118-0acb4395-830a-4bce-bf2f-2a6222e40b6a.png)

비밀번호 : letmein123!

![image](https://user-images.githubusercontent.com/61821641/148638252-4e5913da-d613-43a5-ab9c-bb24def53f5e.png)


#
파일 시스템

NTFS(New Technology File System) 

NTFS 이전에는  FAT16/FAT32 (파일 할당 테이블)와 HPFS (고성능 파일 시스템)가 있었다. 

오늘날에도 FAT 파티션이 사용되는 것을 볼 수 있다. 예를 들어, 일반적으로 USB 장치, MicroSD 카드 등에서 FAT 파티션을 볼 수  있지만 일반적으로 개인용 Windows 컴퓨터/노트북 또는 Windows 서버에서는 볼 수 없다.

NTFS는 저널링 파일 시스템으로 알려져 있다. 오류가 발생한 경우 파일 시스템은 로그 파일에 저장된 정보를 사용하여 디스크의 폴더/파일을 자동으로 복구할 수 있다. 이 기능은 FAT에서는 불가능하다.   

NTFS는 이전 파일 시스템의 많은 제한 사항을 해결한다. 다음과 같은: 

- 4GB보다 큰 파일 지원
- 폴더 및 파일에 대한 특정 권한 설정
- 폴더 및 파일 압축
- 암호화( 암호화 파일 시스템 또는 EFS )


FAT, HPFS 및 NTFS에 대한 Microsoft의 공식 문서는 [여기](https://docs.microsoft.com/en-us/troubleshoot/windows-client/backup-and-storage/fat-hpfs-and-ntfs-file-systems) 에서 읽을 수 있다 . 

NTFS와 관련된 몇 가지 기능에 대해 간략히 설명하겠다. 

NTFS 볼륨에서 파일 및 폴더에 대한 액세스를 허용하거나 거부하는 권한을 설정할 수 있다.

권한은 다음과 같다.

- Full control
- Modify
- Read & Execute
- List folder contents
- Read
- Write
![image](https://user-images.githubusercontent.com/61821641/148643505-7f2946b3-399b-4909-bb23-0e6b566b721b.png)
![image](https://user-images.githubusercontent.com/61821641/148643481-ec7d0c6e-417a-4c73-a75e-9d862d53b58f.png)

NTFS의 또 다른 기능은 ADS ( 대체 데이터 스트림 )이다.

ADS( 대체 데이터 스트림 )는 Windows NTFS (New Technology File System)에 고유한 파일 속성 이다.

모든 파일에는 최소한 하나의 데이터 스트림( $DATA)이 있으며 ADS를 사용하면 파일에 둘 이상의 데이터 스트림이 포함될 수 있다. 기본적으로 Window Explorer 는 사용자에게 ADS를 표시하지 않는다. 이 데이터를 보는 데 사용할 수 있는 타사 실행 파일이 있지만 Powershell 은 파일에 대한 ADS를 볼 수 있는 기능을 제공한다.

보안 관점에서 맬웨어 작성자는 ADS를 사용하여 데이터를 숨겼다.

모든 용도가 악의적인 것은 아니다. 예를 들어, 인터넷에서 파일을 다운로드할 때 파일이 인터넷에서 다운로드되었음을 식별하기 위해 ADS에 기록된 식별자가 있다.

- [ADS](https://blog.malwarebytes.com/101/2015/07/introduction-to-alternate-data-streams/)

#
Windows\System32 폴더

![image](https://user-images.githubusercontent.com/61821641/148643790-44f055e1-c0b3-44c4-b8fd-16a676fcdccc.png)

Windows 폴더(C:\Windows)는 전통적으로 Windows 운영 체제가 포함된 폴더로 알려져 있다.

폴더가 반드시 C 드라이브에 상주할 필요는 없다. 다른 드라이브에 위치할 수 있으며 기술적으로 다른 폴더에 위치할 수 있다.

여기서 환경 변수, 더 구체적으로 시스템 환경 변수가 작동한다. 아직 논의되지 않았지만 윈도우즈 디렉터리의 시스템 환경 변수는 `%windir%`이다.

마이크로소프트는 "환경 변수는 운영 체제 환경에 대한 정보를 저장합니다. 이 정보에는 운영 체제 경로, 운영 체제에서 사용하는 프로세서 수 및 임시 폴더의 위치와 같은 세부 정보가 포함됩니다."

#
사용자 계정, 프로필, 권한

사용자 계정
- Administrator
    - 관리자는 사용자 추가, 사용자 삭제, 그룹 수정, 시스템 설정 수정 등 시스템을 변경할 수 있다. 
- Standard User
    - 표준 사용자는 해당 사용자에게 속한 폴더/파일만 변경할 수 있으며 프로그램 설치와 같은 시스템 수준 변경은 수행할 수 없다.

시스템에 존재하는 사용자 계정을 확인하는 방법

`Start Menu`를 클릭하고 `Other Users`를 검색하면 `System Settings > Other users`가 나타난다.
![image](https://user-images.githubusercontent.com/61821641/148644041-39d66881-c573-4360-b9f0-09950830c2ba.png)

여기서 사용자를 수정하거나 삭제할 수 있다.


사용자 계정을 생성하면 각 사용자 프로필 폴더 위치는 `C:\Users`다. 사용자 프로필 생성은 최초 로그인 시 완료된다. 

각 사용자 폴더에는 동일한 폴더가 있다.
- Desktop
- Documents
- Downloads
- Music
- Pictures

이 정보에 액세스하는 또 다른 방법은 `lusrmgr.msc`를 실행창에서 실행 시키면된다.

![image](https://user-images.githubusercontent.com/61821641/148644247-abdea141-7521-418b-84b5-c1f66faf3843.png)

![image](https://user-images.githubusercontent.com/61821641/148644310-63a8b306-b0b1-4031-8301-2403d768e81e.png)

![image](https://user-images.githubusercontent.com/61821641/148644800-9169a3f2-59b5-4a73-8628-b8954a7b18f8.png)
- Administrator: computer/domain 관리를 위한 기본 제공 계정
- Default Account: 시스템에서 관리하는 사용자 계정
- Guest: computer/domain에 대한 게스트 액세스를 위한 기본 제공 계정
- WDAGUtility Account: Windows Defender Application Guard 시나리오에 대해 시스템에서 관리하고 사용하는 사용자 계정

#
사용자 계정 제어

대부분의 가정 사용자는 Windows 시스템에 로컬 관리자로 로그인한다.

사용자는 인터넷 서핑, Word 문서 작업 등과 같이 이러한 권한이 필요하지 않은 작업을 실행하기 위해 시스템에서 높은(상승된) 권한으로 실행할 필요가 없다. 이 상승된 권한은 시스템의 위험을 증가시킨다. 맬웨어가 시스템을 더 쉽게 감염시킬 수 있기 때문이다. 결과적으로 사용자 계정이 시스템을 변경할 수 있으므로 맬웨어는 로그인한 사용자의 컨텍스트에서 실행된다.

이러한 권한으로 로컬 사용자를 보호하기 위해 Microsoft는 UAC( 사용자 계정 컨트롤)를 도입했다 . 이 개념은 수명이 짧은 Windows Vista 에서 처음 도입되었으며  이후 Windows 버전에서 계속되었다.

참고 : UAC(기본값)는 기본 제공 로컬 관리자 계정에 적용되지 않는다. 

UAC는 어떻게 작동합니까? 계정 유형이 관리자인 사용자가 시스템에 로그인하면 현재 세션이 상승된 권한으로 실행되지 않는다. 더 높은 수준의 권한이 필요한 작업을 실행해야 하는 경우 작업 실행을 허용할지 확인하라는 메시지가 사용자에게 표시된다. 

현재 로그인한 계정의 프로그램을 살펴보겠다. 기본 제공 관리자 계정 - 속성을 보려면 마우스 오른쪽 버튼을 클릭한다.

보안 탭에서 사용자/그룹과 이 파일에 대한 권한을 볼 수 있다. 표준 사용자는 나열되지 않는다. 

#
설정, 제어판

