`impossible password`

```
file impossible_password.bin
```

![image](https://user-images.githubusercontent.com/61821641/150587748-a6632cab-4ba3-449f-b3ef-267b26a51053.png)

64비트 Linux ELF이고 제거되었다는 것을 알고 있습니다. 즉, 디버그 정보가 포함되어 있지 않습니다.

```
chmod +x impossible_password.bin
./impossible_password.bin
```
![image](https://user-images.githubusercontent.com/61821641/150589331-7dbe6090-a328-47f5-98ff-d13d3aa4cb68.png)

```
strings impossible_password.bin
```
![image](https://user-images.githubusercontent.com/61821641/150589053-e194a186-d39b-4f22-98c9-51a950441e06.png)

![image](https://user-images.githubusercontent.com/61821641/150589525-a129f6ce-b45c-4cc1-813a-db527f71792d.png)

```
ltrace ./impossible_password.bin 
```
![image](https://user-images.githubusercontent.com/61821641/150588926-c4f23a44-c384-4ed0-87b0-96b1fa50db6f.png)

`__isoc99_scanf`에서 입력을 읽고 `printf("[%s]\n", "SuperSeKretKey"[SuperSeKretKey]`를 출력한다. 그런 다음 `strcmp("SuperSeKretKey", "SuperSeKretKey")`에서 문자열 비교를 하고 `printf("** ")`를 출력한다.
```
strcmp("SuperSeKretKey", "vCYBKqp~K`yGiv?dr;`:")    
```
두번째 비교문에서 찾은 문자열을 입력해보자.

![image](https://user-images.githubusercontent.com/61821641/150591135-9e649e51-7971-41e0-9823-929813495c6b.png)

문자열이 동적으로 변한다는 것을 알 수 있다.
