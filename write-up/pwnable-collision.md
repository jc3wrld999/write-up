collision
#

![image](https://user-images.githubusercontent.com/61821641/150596924-04ae6f00-997b-4938-b329-5bb106bfd033.png)

```
if(argc<2){
                printf("usage : %s [passcode]\n", argv[0]);
                return 0;
        }
```
![image](https://user-images.githubusercontent.com/61821641/150597059-4e086450-e9bd-4801-aa37-1d1cfad29f9f.png)

```
if(strlen(argv[1]) != 20){
                printf("passcode length should be 20 bytes\n");
                return 0;
        }
```
arg 길이 체크
```
if(hashcode == check_password( argv[1] )){
                system("/bin/cat flag");
                return 0;
        }

```
```
unsigned long hashcode = 0x21DD09EC;
unsigned long check_password(const char* p){
        int* ip = (int*)p;
        int i;
        int res=0;
        for(i=0; i<5; i++){
                res += ip[i];
        }
        return res;
}
```

```
gdb
file col
dissassemble check_password
```
![image](https://user-images.githubusercontent.com/61821641/150598084-31824823-23c4-4cfa-b95e-85d005a07692.png)

